//
//  SchoolDetails.swift
//  20210520-DawitT-NYCSchools
//
//  Created by Dawit Tesfagiorgish on 5/20/21.
//

import Foundation

class SchoolDetails: Decodable {
    let dbn:                                String?
    let school_name:                        String?
    let num_of_sat_test_takers:             String?
    let sat_critical_reading_avg_score:     String?
    let sat_math_avg_score:                 String?
    let sat_writing_avg_score:              String?
}
