//
//  SchoolList.swift
//  20210520-DawitT-NYCSchools
//
//  Created by Dawit Tesfagiorgish on 5/20/21.
//

import Foundation

class SchoolList: Decodable {
    let dbn:                        String?
    let school_name:                String?
    let neighborhood:               String?
    let borough:                    String?
    let finalgrades:                String?
    let total_students:             String?
    let phone_number:               String?
    let school_email:               String?
    let primary_address_line_1:     String?
    let website:                    String?
}
