//
//  APIServices.swift
//  20210520-DawitT-NYCSchools
//
//  Created by Dawit Tesfagiorgish on 5/20/21.
//

import Foundation

public class APIServices {
    
    private let urlSession: URLSession
    //Hard Code the params for now, it should have been implemented with a proper HTTP request methods
    static let schoolsEndPoint = "https://data.cityofnewyork.us/resource/s3k6-pzi2.json?$select=dbn,school_name,neighborhood,borough,finalgrades,total_students,phone_number,school_email,primary_address_line_1,website"
    static let schoolDetailsEndPoint = "https://data.cityofnewyork.us/resource/f9bf-2cp4.json?dbn="
    
    init(urlSession: URLSession = .shared) {
        self.urlSession = urlSession
    }
    
    func performRequestForSchoolList(urlString: String, onResult completion: @escaping ([SchoolList], Error?) -> Void) {
        
        if let url = URL(string: urlString) {
            let task = urlSession.dataTask(with: url) { (data, response, error) in
                do {
                    let result = try JSONDecoder().decode([SchoolList].self, from: data!)
                    completion(result, error)
                } catch {
                    completion([], error)
                }
            }
            task.resume()
        }
    }
    
    func performRequestForSchoolDetails(urlString: String, onResult completion: @escaping ([SchoolDetails], Error?) -> Void) {
        
        if let url = URL(string: urlString) {
            let task = urlSession.dataTask(with: url) { (data, response, error) in
                do {
                    let result = try JSONDecoder().decode([SchoolDetails].self, from: data!)
                    completion(result, error)
                } catch {
                    completion([], error)
                }
            }
            task.resume()
        }
    }
}
