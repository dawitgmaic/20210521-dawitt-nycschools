//
//  NYCSchoolsViewController.swift
//  20210520-DawitT-NYCSchools
//
//  Created by Dawit Tesfagiorgish on 5/20/21.
//

import UIKit

class NYCSchoolsViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    
    private var apiServices: APIServices?
    private var schoolList: [SchoolList]?
    private var schoolDetail: SchoolDetails?
    private var selectedIndex = 0
    private let spinnerView = UIActivityIndicatorView()
    
    private let configuration = URLSessionConfiguration.default
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        
        //Sort functionality is very necessary in  this specific case, would have implemented for this, to sort filter by Borough, Grade...
        navigationItem.title = "NYC Schools"
        tableView.register(UINib(nibName: String(describing: SchoolsTableViewCell.self), bundle: nil), forCellReuseIdentifier: "SchoolsTableViewCell")
        tableView.dataSource = self
        tableView.delegate = self
        tableView.isHidden = true
        view.addSubview(spinnerView)
        spinnerView.startAnimating()
        
        //Api Service setting - Best practices - should be moved to common network call files
        configuration.requestCachePolicy = .reloadIgnoringLocalCacheData
        apiServices = APIServices(urlSession: URLSession(configuration: configuration))
        //Initial service call
        getSchools()
    }
    
    // MARK: - TableView - Delegate & DataSource Methods
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return schoolList?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: SchoolsTableViewCell.self), for: indexPath) as? SchoolsTableViewCell else {
            return UITableViewCell()
        }
        if let school = schoolList?[indexPath.row] {
            cell.schoolName.text = school.school_name
            cell.borough.text = (school.neighborhood ?? "") + ", " + (school.borough ?? "") // long string construction just to show both for now
            cell.finalGrades.text = school.finalgrades
            cell.numberOfStudents.text = school.total_students
            cell.phoneNumber.text = school.phone_number
        }
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let dbn = schoolList?[indexPath.row].dbn {
            getSchoolDetails(dbn: dbn)
        }
        selectedIndex = indexPath.row
    }
    
    // MARK: - Service Call
    
    private func getSchools() {
        // simple spinner view for now, would have customized with time
        spinnerView.startAnimating()
        
        // I would have implemented the service call with limit and pagination in order to avoid big data load time - For now I am not passign those filter data but fetching very few data's only for all schools. But defintely PAGINATION is required in this case
        apiServices?.performRequestForSchoolList(urlString: APIServices.schoolsEndPoint, onResult: {[weak self] (schoolList, error) in
            guard let strongSelf = self  else {
                print("Error - weak self is nil")
                return
            }
            
            if error != nil {
                strongSelf.alertViews(false)
                return
            }
            
            strongSelf.schoolList = schoolList
            strongSelf.processResponse()
        })
    }
    
    private func getSchoolDetails(dbn: String) {
        let fullEndpoint = APIServices.schoolDetailsEndPoint + dbn
        apiServices?.performRequestForSchoolDetails(urlString: fullEndpoint, onResult: {[weak self] (schoolDetail, error) in
            guard let strongSelf = self  else {
                print("Error - weak self is nil")
                return
            }
            if error != nil {
                strongSelf.alertViews(true)
                return
            }
            strongSelf.navigateToDetails(schoolDetails: schoolDetail)
        })
    }
    
    // MARK: - Helper Func
    
    private func processResponse() {
        DispatchQueue.main.async {
            self.tableView.isHidden = false
            self.spinnerView.stopAnimating()
            self.tableView.reloadData()
        }
    }
    
    private func navigateToDetails(schoolDetails: [SchoolDetails]) {
        DispatchQueue.main.async {
            if schoolDetails.count == 0 {
                self.alertViews(true)
            } else if let school = self.schoolList?[self.selectedIndex] {
                let vc = SchoolDetailsViewContoller.make(school: school, schoolDetails: schoolDetails)
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    
    private func alertViews(_ isForDetails: Bool) {
        //Alert to handle the Error path for both service calls
        DispatchQueue.main.async {
            let title = isForDetails ? "School Details" : "NYC School"
            let alert = UIAlertController(title: title,message: "Opps, Sorry we are not able to find the details at a moment", preferredStyle: .alert)
            let buttonTitle = isForDetails ? "Ok" : "Try Again"
            let actionButton = UIAlertAction(title: buttonTitle, style: .default) { UIAlertAction in
                isForDetails ? nil : self.getSchools()
            }
            alert.addAction(actionButton)
            self.present(alert, animated: true, completion:  nil)
        }
    }
}

