

import Foundation
import UIKit

class SchoolDetailsViewContoller: UIViewController, UITextFieldDelegate, UITextViewDelegate {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var overlayScoreView: UIView!
    @IBOutlet weak var mathScore: UILabel!
    @IBOutlet weak var readingScore: UILabel!
    @IBOutlet weak var writingScore: UILabel!
    
    @IBOutlet weak var overlayContactsView: UIView!
    @IBOutlet weak var address: UILabel!
    @IBOutlet weak var email: UILabel!
    @IBOutlet weak var website: UILabel!
    
    private var schoolDetail: SchoolDetails!
    private var schoolList: SchoolList!
    
    
    static func make(school: SchoolList, schoolDetails: [SchoolDetails]) -> UIViewController {
        if let viewController = UIStoryboard(name: "SchoolDetails", bundle: nil).instantiateViewController(withIdentifier: String(describing: SchoolDetailsViewContoller.self)) as? SchoolDetailsViewContoller {
            viewController.schoolDetail = schoolDetails[0]
            viewController.schoolList = school
            return viewController
        }
        return UIViewController()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = "School Details"
        setUpViewsAndData()
    }
    
    
    private func setUpViewsAndData() {
        overlayScoreView.layer.cornerRadius = 16
        overlayContactsView.layer.cornerRadius = 16
        
        titleLabel.text = schoolList.school_name
        
        // SAT Score Overlay UI Data
        mathScore.text = schoolDetail.sat_math_avg_score
        readingScore.text = schoolDetail.sat_critical_reading_avg_score
        writingScore.text = schoolDetail.sat_writing_avg_score
        
        // Contacts Overlay UI Data
        address.text = schoolList.primary_address_line_1
        email.text = schoolList.school_email
        website.text = schoolList.website
    }
}




