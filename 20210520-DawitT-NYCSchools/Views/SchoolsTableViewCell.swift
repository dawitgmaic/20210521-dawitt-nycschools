//
//  SchoolsTableViewCell.swift
//  20210520-DawitT-NYCSchools
//
//  Created by Dawit Tesfagiorgish on 5/20/21.
//

import UIKit

class SchoolsTableViewCell: UITableViewCell {

    @IBOutlet weak var schoolName: UILabel!
    @IBOutlet weak var borough: UILabel!
    @IBOutlet weak var finalGrades: UILabel!
    @IBOutlet weak var numberOfStudents: UILabel!
    @IBOutlet weak var phoneNumber: UILabel!
    
    //Make cells and view reusalbe - by implementing configuration func and styles within them - Ok for now with limited time
}
